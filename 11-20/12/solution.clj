(use 'clojure.math.numeric-tower)

(defn is-factor? [candidate number]
  (= (rem number candidate) 0))

(defn is-prime? [number]
  (let [square-root (sqrt number)]
    (loop [factor 2]
      (if (> factor square-root)
        true
        (if (= (rem number factor) 0)
          false
          (recur (+ factor 1)))))))

(defn get-primes-up-to [limit]
  (loop [candidate 3 primes [2]]
    (if (> candidate limit)
      primes
      (let [next-candidate (+ candidate 2)]
        (if (is-prime? candidate)
          (recur next-candidate (conj primes candidate))
          (recur next-candidate primes))))))

(defn get-prime-factors [number]
  (loop [primes (get-primes-up-to number) prime-factors []]
    (if (= (count primes) 0)
      prime-factors
      (let [prime (first primes) remaining-primes (drop 1 primes)]
        (if (is-factor? prime number)
          (recur remaining-primes (conj prime-factors prime))
          (recur remaining-primes prime-factors))))))

(defn get-factor-exponent [number factor]
  (loop [product number exponent 0]
    (if (= (rem product factor) 0)
      (recur (/ product factor) (+ exponent 1))
      exponent)))

(defn get-prime-factor-exponents [number]
  (map #(get-factor-exponent number %) (get-prime-factors number)))

(defn get-number-of-divisors [number]
  (if (= number 1)
    1
    (reduce #(* %1 (+ %2 1)) 1 (get-prime-factor-exponents number))))

(defn get-first-triangle-number-with-n-divisors [n]
  (loop [triangle 1 naturals (drop 2 (range))]
    (if (>= (get-number-of-divisors triangle) n)
      triangle
      (recur (+ triangle (first naturals)) (drop 1 naturals)))))

(println (get-first-triangle-number-with-n-divisors 501))

