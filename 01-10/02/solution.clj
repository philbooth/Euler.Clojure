(defn sum-even-fibonacci [limit]
  (loop [first 0 second 1 sum 0]
    (if (<= second limit)
      (let [new-sum
        (if (= (rem second 2) 0)
          (+ sum second)
          sum)]
        (recur second (+ first second) new-sum))
      sum)))

(println (sum-even-fibonacci 4000000))

