(defn is-evenly-divisible? [number factors]
  (reduce
    #(if (= %1 false)
      false
      (= (rem number %2) 0))
    true factors))

(defn get-smallest-evenly-divisible-number [divisors]
  (let [greatest-divisor (last divisors)]
    (loop [candidate greatest-divisor]
      (if (is-evenly-divisible? candidate divisors)
        candidate
        (recur (+ candidate greatest-divisor))))))

(println (get-smallest-evenly-divisible-number (range 1 21)))
  
