(defn sum-multiples [divisors limit]
  (reduce
    (fn [sum candidate]
      (if (some #(= (rem candidate %) 0) divisors)
        (+ sum candidate)
        sum))
    0 (range 1 limit)))

(println (sum-multiples [3 5] 1000))

